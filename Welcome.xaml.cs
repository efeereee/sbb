﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SBSCTEST.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SBSCTEST
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Welcome : ContentPage
    {
        private HttpClient _client = new HttpClient();


        public Welcome()
        {
            InitializeComponent();
            GetNews();

        }

     

        private void LsNews_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            LsNews.SelectedItem=null;
        }

        private void LsNews_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            Application.Current.Properties["NewsItem"] = (Article)e.Item;
            ;

            Navigation.PushAsync(new DataPage()
           {
               Title = "News in Details"
           });

        }




        protected async void GetNews()
        {

            dataLoading.IsRunning = true;
            dataLoading.IsVisible = true;
            var uri = new Uri("https://newsapi.org/v2/top-headlines?country=us&apiKey=daf0c4c3fde143f2a1a6b648852cc482", UriKind.Absolute);
            var response = await _client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                dataLoading.IsRunning = false;
                dataLoading.IsVisible = false;
                var newsData=(JsonConvert.DeserializeObject<NewsModel>(content)).articles;
                await DisplayAlert("SUCCESS", "Data Successfully synced", "OK");
                dataLoading.IsRunning = false;
                LsNews.ItemsSource = newsData;
           


            }
            else
            {
                var errorJson = await response.Content.ReadAsStringAsync();
                await DisplayAlert("ERROR", errorJson, "OK");
                dataLoading.IsRunning = false;
                dataLoading.IsVisible = false;
            }


        }

        private void LsNews_OnRefreshing(object sender, EventArgs e)
        {
            GetNews();
        }
    }
}