﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBSCTEST.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SBSCTEST
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DataPage : ContentPage
    {
        public DataPage()
        {
            InitializeComponent();

            LblTitle.Text = ((Article)Application.Current.Properties["NewsItem"]).title;
            LblDetails.Text = ((Article)Application.Current.Properties["NewsItem"]).description;
            NewsImage.Source = ((Article)Application.Current.Properties["NewsItem"]).urlToImage;

        }

        private void Button_OnClicked(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        }
    }
}